<?php
function puffinpress_setup(){
	/* translate theme */
	
	load_theme_textdomain('puffinpress');
	
	add_theme_support('title-tag');
	
	add_theme_support('post-thumbnails');
	
	add_image_size('slider-image', 705, 660, true);
	add_image_size('post-thumb', 450, 301, true);
	add_image_size('puffin-recent-thumbnail', 100, 67, true);
	add_image_size('puffin-single-post-thumb', 930, 400, true);
	
	add_theme_support('custom-logo', array(
		'height' => 45,
		'width' => 139,
		'flex-height' => true,
		'flex-wide' => true
		
	));
	